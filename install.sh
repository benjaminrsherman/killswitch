#!/bin/sh

mkdir -p $HOME/.config/systemd/user/

cp killswitch.service $HOME/.config/systemd/user/killswitch.service

systemctl enable --now --user killswitch.service
