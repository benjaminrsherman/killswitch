import discord
import subprocess
import sys

SELF_ID = int(sys.argv[2])

client = discord.Client()


@client.event
async def on_message(msg):
    if msg.author.id == SELF_ID:
        subprocess.run(["shutdown", "now"])


client.run(sys.argv[1])
